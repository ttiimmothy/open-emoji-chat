import express from "express";
import {isLoggedInAPI} from "../lib/guard";
import {
    userController,
    findController,
    likeController,
    activityController,
    groupController,
    friendController,
    inviteController,
    profileController,
} from "../main";
import {upload} from "../lib/multer";

export const userRoutes = express.Router();
export const findRoutes = express.Router();
export const likeRoutes = express.Router();
export const inviteRoutes = express.Router();
export const activityRoutes = express.Router();
export const groupRoutes = express.Router();
export const friendRoutes = express.Router();
export const profileRoutes = express.Router();
// const asyncMiddleware = (fn: any) => async (req: express.Request, res: express.Response, next: express.NextFunction) =>
//     Promise.resolve(fn(req, res, next)).catch((err) => {
//         logger.error(err);
//         res.status(500).json({message: "internal server error"});
//     });

// 遲過controller同service先run
export function initRoute() {
    userRoutes.put("/checkUuid", userController.checkUuid);
    userRoutes.post("/sendVerify", userController.sendVerify);
    userRoutes.post("/completeRegister", upload.single("icon"), userController.completeRegister);
    userRoutes.post("/login", userController.login);
    userRoutes.get("/currentUser", userController.currentUser);
    userRoutes.get("/login/google", userController.loginGoogle);
    userRoutes.get("/login/twitter", userController.loginTwitter);
    userRoutes.get("/login/facebook", userController.loginFacebook);
    userRoutes.post("/forgetPassword", userController.forgetPassword);
    userRoutes.post("/checkEmail", userController.checkEmail);
    userRoutes.put("/getUsernameResetPassword", userController.getUsernameResetPassword);
    userRoutes.get("/getAllUsers", userController.getAllUsers);
    userRoutes.put("/resetPassword", userController.resetPassword);
    userRoutes.get("/logout", userController.logout);

    findRoutes.get("/showAvailable", isLoggedInAPI, findController.showAvailable);
    findRoutes.post("/currentLocation", isLoggedInAPI, findController.currentLocation);
    findRoutes.get("/getOtherUsersLatLon", isLoggedInAPI, findController.getOtherUsersLatLon);

    likeRoutes.get("/showUsers/:id", isLoggedInAPI, likeController.showUsers);
    likeRoutes.post("/likeUser/:id", isLoggedInAPI, upload.single("content"), likeController.sendRecording);
    likeRoutes.post("/sendEmojiRecording", isLoggedInAPI, upload.single("content"), likeController.sendEmojiRecording);
    likeRoutes.delete("/rejectUser/:id", isLoggedInAPI, likeController.rejectTarget);

    inviteRoutes.get("/showInviteReceived", isLoggedInAPI, inviteController.showInviteReceived);
    inviteRoutes.get("/showInviteSent", isLoggedInAPI, inviteController.showInviteSent);

    activityRoutes.post("/newActivity", isLoggedInAPI, activityController.addNewActivity);
    activityRoutes.get("/joinActivity/:activityID", isLoggedInAPI, activityController.joinActivity);
    activityRoutes.get("/confirmApplication/:userID/:activityID", isLoggedInAPI, activityController.confirmApplication);
    activityRoutes.get("/appointedActivities", isLoggedInAPI, activityController.appointedActivity);
    activityRoutes.get("/toConfirmActivity", isLoggedInAPI, activityController.toConfirmActivity);
    activityRoutes.get("/waitingActivity", isLoggedInAPI, activityController.waitingActivity);
    activityRoutes.get("/activityList", isLoggedInAPI, activityController.activityList);
    activityRoutes.get("/pastActivity", isLoggedInAPI, activityController.pastActivity);
    activityRoutes.get("/completeActivity/:activityId", isLoggedInAPI, activityController.completeActivity);
    activityRoutes.get("/cancelActivities/:activityId", isLoggedInAPI, activityController.cancelActivities);
    activityRoutes.post("/newActivityMessage", isLoggedInAPI, activityController.sendActivityMessage);
    activityRoutes.get("/showChatUsers/:userId", isLoggedInAPI, activityController.showChatUsers);
    activityRoutes.get("/activityChat/:activityId", isLoggedInAPI, activityController.activityChat);

    groupRoutes.post("/createGroup", isLoggedInAPI, upload.single("group_icon"), groupController.addGroup);
    groupRoutes.get("/groupList", isLoggedInAPI, groupController.groupList);
    groupRoutes.get("/checkInGroup", isLoggedInAPI, groupController.checkInGroup);
    groupRoutes.get("/checkInSpecificGroup/:id", isLoggedInAPI, groupController.checkInSpecificGroup);
    groupRoutes.get("/groupWaitInfoList", isLoggedInAPI, groupController.groupWaitInfoList);
    groupRoutes.post("/joinGroup", isLoggedInAPI, groupController.joinGroup);
    groupRoutes.get("/groupName/:id", isLoggedInAPI, groupController.groupName);
    groupRoutes.get("/groupInfo/:id", isLoggedInAPI, groupController.groupInfo);
    groupRoutes.get("/groupWaitInfo/:id", isLoggedInAPI, groupController.groupWaitInfo);
    groupRoutes.get("/getGroupChat/:id", isLoggedInAPI, groupController.getGroupChat);
    groupRoutes.get("/checkAdmin/:id", isLoggedInAPI, groupController.checkAdmin);
    groupRoutes.post("/addAdmin/:id", isLoggedInAPI, groupController.addAdmin);
    groupRoutes.post("/addToGroup/:id", isLoggedInAPI, groupController.addToGroup);
    groupRoutes.put("/changeGroup/:id", isLoggedInAPI, upload.single("group_icon"), groupController.changeGroup);
    groupRoutes.delete("/deleteFromWaiting/:id", isLoggedInAPI, groupController.deleteFromWaiting);
    groupRoutes.post(
        "/sendGroupMessage/:id",
        isLoggedInAPI,
        upload.single("content"),
        groupController.sendGroupMessage
    );

    friendRoutes.get("/getFriendList", isLoggedInAPI, friendController.getFriendList);
    friendRoutes.get("/getChat/:id", isLoggedInAPI, friendController.getChat);
    friendRoutes.post("/sendMessage/:id", isLoggedInAPI, upload.single("content"), friendController.sendMessage);
    friendRoutes.put("/searchMessage", isLoggedInAPI, friendController.searchMessage);

    profileRoutes.get("/getProfile", isLoggedInAPI, profileController.getProfile);
    profileRoutes.get("/hobbyList", isLoggedInAPI, profileController.hobbyList);
    profileRoutes.post(
        "/updateProfileBasic",
        isLoggedInAPI,
        upload.single("icon"),
        profileController.updateProfileBasic
    );
    profileRoutes.post(
        "/updateProfileHobby",
        isLoggedInAPI,
        upload.single("icon"),
        profileController.updateProfileHobby
    );
}
