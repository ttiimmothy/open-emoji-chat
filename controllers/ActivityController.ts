import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {ActivityService} from "../services/ActivityService";
import {Server as SocketIO} from "socket.io";

export class ActivityController {
    constructor(private activityService: ActivityService, private io: SocketIO) {}

    addNewActivity = async (req: Request, res: Response) => {
        try {
            let activities = req.body;
            activities.host_id = req.session["user"].id;
            if (req.body) {
                await this.activityService.addNewActivity(activities);
                res.status(200).json({success: true});
                this.io.emit("addNewActivity");
            } else {
                res.status(400).json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    joinActivity = async (req: Request, res: Response) => {
        try {
            let user_id = parseInt(req.session["user"].id);
            let activityID = parseInt(req.params.activityID);
            await this.activityService.joinActivity(activityID, user_id);
            res.status(200).json({success: true});
            this.io.emit("joinActivity");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    confirmApplication = async (req: Request, res: Response) => {
        try {
            logger.debug("confirmApplication");
            let userID = parseInt(req.params.userID);
            let activityID = parseInt(req.params.activityID);
            await this.activityService.confirmApplication(userID, activityID);
            res.status(200).json({success: true});
            this.io.emit("confirmApplication");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
    completeActivity = async (req: Request, res: Response) => {
        try {
            let activityId = parseInt(req.params.activityId);
            await this.activityService.completeActivity(activityId);
            res.status(200).json({success: true});
            this.io.emit("completeActivity");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
    cancelActivities = async (req: Request, res: Response) => {
        try {
            let activityId = parseInt(req.params.activityId);
            await this.activityService.cancelActivities(activityId);
            res.status(200).json({success: true});
            this.io.emit("cancelActivities");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    appointedActivity = async (req: Request, res: Response) => {
        try {
            let user_id = parseInt(req.session["user"].id);
            let activityList = await this.activityService.appointedActivity(user_id);
            res.status(200).json(activityList);
        } catch (e) {
			logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    toConfirmActivity = async (req: Request, res: Response) => {
        try {
            let user_id = parseInt(req.session["user"].id);
            let activityList = await this.activityService.toConfirmActivityList(user_id);
            res.status(200).json(activityList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    waitingActivity = async (req: Request, res: Response) => {
        try {
            let user_id = parseInt(req.session["user"].id);
            let activityList = await this.activityService.waitingActivity(user_id);
            res.status(200).json(activityList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    activityList = async (req: Request, res: Response) => {
        try {
            let user_gender = parseInt(req.session["user"].gender);
            let user_id = parseInt(req.session["user"].id);
            let activityList = await this.activityService.activityList(user_id, user_gender);
            res.status(200).json(activityList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    pastActivity = async (req: Request, res: Response) => {
        try {
            let user_id = parseInt(req.session["user"].id);
            let activityList = await this.activityService.pastActivityList(user_id);
            res.status(200).json(activityList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendActivityMessage = async (req: Request, res: Response) => {
        try {
            let activity_id = req.body.activity_id;
            let participant_id = parseInt(req.session["user"].id);
            let content = req.body.content;
            await this.activityService.newActivityMessage(activity_id, participant_id, content);
            res.json({success: true});
            this.io.emit("new-message");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    showChatUsers = async (req: Request, res: Response) => {
        try {
            const userId = parseInt(req.params.userId);
            let user = await this.activityService.showChatUsers(userId);
            res.json(user);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    activityChat = async (req: Request, res: Response) => {
        try {
            const activityId = parseInt(req.params.activityId);
            let chat = await this.activityService.activityChat(activityId);
            res.json(chat);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
