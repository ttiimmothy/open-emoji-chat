import numpy as np
import keras
import time
import librosa
import os
import matplotlib.pyplot as plt
import tensorflow as tf
import csv
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dropout, Activation
from keras.layers import Conv1D, MaxPooling1D
from keras.models import Model
from keras.callbacks import ModelCheckpoint
import sys
import librosa
import bulkDiarize as bk
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
model = keras.models.load_model(
    'model/lstm_cnn_rectangular_lowdropout_trainedoncustomdata.h5')

classes = ['Neutral', 'Happy', 'Sad',
           'Angry', 'Fearful', 'Disgusted', 'Surprised']

def predict(folder, classes, model, messageToEmoji):
    solutions = []
    filenames = []
    for subdir in ["messages"]:
        lst = []
        predictions = []
        filenames.append(subdir)
        my_folder = os.path.join(folder, subdir, messageToEmoji)
        for file in os.listdir(my_folder):
            if file.endswith('.wav'):
                filename = os.path.join(my_folder, file)
                temp = np.zeros((1, 13, 216))
                X, sample_rate = librosa.load(
                    filename, res_type='kaiser_fast', duration=2.5, sr=22050 * 2, offset=0.5)
                mfccs = librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13)
                result = np.zeros((13, 216))
                result[:mfccs.shape[0], :mfccs.shape[1]] = mfccs
                temp[0] = result
                t = np.expand_dims(temp, axis=3)
                ans = model.predict_classes(t)
                predictions.append(classes[ans[0]])

        if len(predictions) < 2:
            predictions.append('None')
        solutions.append(predictions)
    return solutions, filenames

def emojiModel(messageToEmoji):
    # /Users/kc/Desktop/Tecky/BAD_Project/tecky-project/python/mevonAISpeechEmotionRecognitionmaster/src
    cwd = os.getcwd()
    cwd = cwd.split('/')
    cwd_length = len(cwd)
    target_directory = []
    for i in range(0, cwd_length - 3):
        target_directory.append(cwd[i])
    target_directory.append("private")

    def listToString(s):
        str1 = "/"
        return (str1.join(s))
    target_directory = listToString(target_directory)
    INPUT_FOLDER_PATH = os.path.abspath(target_directory)
    # INPUT_FOLDER_PATH = os.chdir("../../../private/")
    OUTPUT_FOLDER_PATH = "output/"
    # bk.diarizeFromFolder(INPUT_FOLDER_PATH,OUTPUT_FOLDER_PATH)
    for subdir in ["messages"]:
        bk.diarizeFromFolder(f'{INPUT_FOLDER_PATH}{"/"}{subdir}{"/"}',
                             (f'{OUTPUT_FOLDER_PATH}{subdir}{"/"}'), messageToEmoji)

    folder = OUTPUT_FOLDER_PATH
    for subdir in ["messages"]:
        predictions, filenames = predict(
            f'{folder}', classes, model, messageToEmoji)
        with open('SER_' + subdir + '.csv', 'w') as csvFile:
            writer = csv.writer(csvFile)
            for i in range(len(filenames)):
                csvData = [filenames[i], 'person01', predictions[i]
                           [0], 'person02', predictions[i][1]]
                print(",Predicted Emotion := Person1:",
                      predictions[i][0])
                writer.writerow(csvData)
                return predictions[i][0]
        csvFile.close()
    os.remove("filterTemp.wav")

# emojiModel("content-1621064390451")
