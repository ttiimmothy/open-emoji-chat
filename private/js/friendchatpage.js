let urlParams = new URLSearchParams(window.location.search);
let urlID = urlParams.get("id");
//webkitURL is deprecated but nevertheless
let url = window.URL || window.webkitURL;
let gumStream; //stream from getUserMedia()
let rec; //Recorder.js object
let input; //MediaStreamAudioSourceNode we'll be recording
// shim for AudioContext when it's not avb.
let AudioContext = window.AudioContext || window.webkitAudioContext;
let audioContext; //audio context to help us record
let recordButton = document.getElementById("recordButton");
let stopButton = document.getElementById("stopButton");
let socket;

function initSocket() {
    socket = io.connect();
}
initSocket();

socket.on("new-message", async function () {
    await getChatHistory();
    let bottom = document.querySelector(".chat-area");
    bottom.scrollTop = bottom.scrollHeight;
});

async function getProfile() {
    const res = await fetch("/showUsers/" + urlID);
    const result = await res.json();
    const targetUser = result[0][0];

    document.querySelector(".user-data").innerHTML = /*html*/ ``;
    document.querySelector(".user-data").innerHTML += /*html*/ `
	<div class="user-icon"><img class="icon-XXsmall" src="../uploads/${targetUser.icon}"></div>
	<div class="user-name-message">
		<div class="user-name">${targetUser.display_name}</div>
		<div class="user-message-new">${targetUser.description}</div>
	</div>`;
}

async function getChatHistory() {
    const res = await fetch("/getChat/" + urlID);
    const friendHistories = await res.json();
    document.querySelector(".chat-area").innerHTML = /*html*/ ``;
    let position;
    if (friendHistories.length > 0) {
        for await (let friendHistory of friendHistories) {
            if (parseInt(urlID) === friendHistory.user_id) {
                position = "receive";
            } else {
                position = "send";
            }
            const dateAndTime = new Date(friendHistory.created_at);
            let time =
                ("0" + (dateAndTime.getDate() + 1)).slice(-2) +
                "/" +
                ("0" + (dateAndTime.getMonth() + 1)).slice(-2) +
                " " +
                ("0" + dateAndTime.getHours()).slice(-2) +
                ":" +
                ("0" + dateAndTime.getMinutes()).slice(-2);

            document.querySelector(".chat-area").innerHTML += /*html*/ `
            <div class="user-data-chat-${position}">
                <div class="user-name-message user-chat-${position}">
					<div>
						<div><audio src="../messages/${friendHistory.content}" controls></audio></div>
						<div class="message-time">${time}</div>
					</div>
                </div>
            </div>`;
            let bottom = document.querySelector(".chat-area");
            bottom.scrollTop = bottom.scrollHeight;
        }
    } else {
        document.querySelector(".chat-area").innerHTML = /*html*/ `
        <div class="empty-chat">
            <div>配對成功😍<div>
            <div>「點擊錄音」繼續對話<div>
        <div>`;
    }
}

function startRecording() {
    /** simple constraints object, for more advanced audio features see
    https://addpipe.com/blog/audio-constraints-getusermedia/
    */
    let constraints = {audio: true, video: false};

    // disable the record button until we get a success or fail from getUserMedia()
    recordButton.hidden = true;
    stopButton.hidden = false;

    /** we're using the standard promise based getUserMedia()
    https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    */

    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(function (stream) {
            /** create an audio context after getUserMedia is called
            sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
            the sampleRate defaults to the one set in your OS for your playback device
            */
            audioContext = new AudioContext();

            // assign to gumStream for later use
            gumStream = stream;

            // use the stream
            input = audioContext.createMediaStreamSource(stream);

            /** create the Recorder object and configure to record mono sound (1 channel)
            recording 2 channels will double the file size
            */
            rec = new Recorder(input, {numChannels: 1});

            // start the recording process
            rec.record();
        })
        .catch(function () {
            // enable the record button if getUserMedia() fails
            recordButton.hidden = false;
            stopButton.hidden = true;
        });
}

function stopRecording() {
    // disable the stop button, enable the record too allow for new recordings
    stopButton.hidden = true;
    recordButton.hidden = false;

    // tell the recorder to stop the recording
    rec.stop();

    // stop microphone access
    gumStream.getAudioTracks()[0].stop();

    // create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(fetchMessage);
}

function fetchMessage(blob) {
    let filename = new Date().toISOString();
    let friend = new FormData();
    friend.append("content", blob, filename);
    fetch("/sendMessage/" + urlID, {
        method: "post",
        body: friend,
    });
}

// add events to these 2 buttons
recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);

window.onload = async function () {
    getProfile();
    getChatHistory();
};
