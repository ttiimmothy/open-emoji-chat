export async function getCurrentUser() {
    const res = await fetch("/currentUser");
    const result = await res.json();

    if (result.success) {
        window.location.href = "find.html";
    }
}
