export let urlParams = new URLSearchParams(window.location.search);
export let urlID = urlParams.get("id");
export let recordButton = document.getElementById("recordButton");
export let stopButton = document.getElementById("stopButton");
export let socket;
let instruction = document.querySelector(".instruction");
//webkitURL is deprecated but nevertheless
let url = window.URL || window.webkitURL;
let gumStream; //stream from getUserMedia()
let rec; //Recorder.js object
let input; //MediaStreamAudioSourceNode we'll be recording
let AudioContext = window.AudioContext || window.webkitAudioContext;
let audioContext; //audio context to help us records

function initSocket() {
    socket = io.connect();
}
initSocket();
instruction.innerText = /*html*/ `開始錄音`;

export async function getChatHistory() {
    const res = await fetch("/getGroupChat/" + urlID);
    const groupHistories = await res.json();

    document.querySelector(".chat-area").innerHTML = /*html*/ ``;
    let position;
    if (groupHistories.groupMessage.length > 0) {
        for await (let groupHistory of groupHistories.groupMessage) {
            if (groupHistories.currentUserId === parseInt(groupHistory.member_id)) {
                position = "send";
            } else {
                position = "receive";
            }
            const dateAndTime = new Date(groupHistory.created_at);
            let time =
                ("0" + (dateAndTime.getDate() + 1)).slice(-2) +
                "/" +
                ("0" + (dateAndTime.getMonth() + 1)).slice(-2) +
                " " +
                ("0" + dateAndTime.getHours()).slice(-2) +
                ":" +
                ("0" + dateAndTime.getMinutes()).slice(-2);

            document.querySelector(".chat-area").innerHTML += /*html*/ `
            <div class="group-data-chat-${position}">
                <div class="group-name-message group-chat-${position}">
					${position === "receive" ? `<img class='memberIcon img-fluid' src='../uploads/${groupHistory.icon}'>` : ""}
                    <div>
						<audio src="../messages/${groupHistory.content}" controls></audio>
						${
                            position === "receive"
                                ? `<div class="message-name-time">
									<div>${groupHistory.display_name}</div>
									<div>${time}</div>
								</div>`
                                : `<div class="message-time">${time}</div>`
                        }
						</div>
					</div>
                </div>
            </div>`;
            let bottom = document.querySelector(".chat-area");
            bottom.scrollTop = bottom.scrollHeight;
        }
    }
}

export function startRecording() {
    /** simple constraints object, for more advanced audio features see
    https://addpipe.com/blog/audio-constraints-getusermedia/
    */
    let constraints = {audio: true, video: false};

    // disable the record button until we get a success or fail from getUserMedia()
    recordButton.hidden = true;
    stopButton.hidden = false;
    instruction.innerText = /*html*/ `停止錄音`;

    /** we're using the standard promise based getUserMedia()
    https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    */

    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(function (stream) {
            /** create an audio context after getUserMedia is called
            sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
            the sampleRate defaults to the one set in your OS for your playback device
            */
            audioContext = new AudioContext();

            // assign to gumStream for later use
            gumStream = stream;

            // use the stream
            input = audioContext.createMediaStreamSource(stream);

            /** create the Recorder object and configure to record mono sound (1 channel)
            recording 2 channels will double the file size
            */
            rec = new Recorder(input, {numChannels: 1});

            // start the recording process
            rec.record();
        })
        .catch(function () {
            // enable the record button if getUserMedia() fails
            recordButton.hidden = false;
            stopButton.hidden = true;
        });
}

export function stopRecording() {
    // disable the stop button, enable the record too allow for new recordings
    stopButton.hidden = true;
    recordButton.hidden = false;
    instruction.innerText = /*html*/ `開始錄音`;

    // tell the recorder to stop the recording
    rec.stop();

    // stop microphone access
    gumStream.getAudioTracks()[0].stop();

    // create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(fetchMessage);
}

export async function fetchMessage(blob) {
    let filename = new Date().toISOString();

    let group = new FormData();
    group.append("content", blob, filename);
    await fetch("/sendGroupMessage/" + urlID, {
        method: "post",
        body: group,
    });
}

// // add events to those 2 buttons
recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
