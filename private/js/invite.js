import {currentLocation} from "./updateLocation.js";
const displayBy = document.querySelectorAll(".display-by");
const displayByReceived = document.querySelector("#display-by-received");
const displayBySent = document.querySelector("#display-by-sent");
const mainContainer = document.querySelector(".main-container");
let genderIcon;

function sortBy() {
    if (parseInt(displayByReceived.getAttribute("data-ascension")) === 1) {
        displayByReceivedFunction();
    }
    if (parseInt(displayBySent.getAttribute("data-ascension")) === 1) {
        displayBySentFunction();
    }
}

function setSorting() {
    displayBy.forEach((element) => {
        element.addEventListener("click", () => {
            const removedArray = [...displayBy].filter((filtering) => {
                return filtering !== element;
            });
            removedArray.forEach((element) => {
                element.setAttribute("data-ascension", "0");
                element.classList.remove("active");
            });
            if (parseInt(element.getAttribute("data-ascension")) === 1) {
                element.setAttribute("data-ascension", "-1");
            } else {
                element.setAttribute("data-ascension", "1");
            }
            element.classList.add("active");
            sortBy();
        });
    });
}

async function displayByReceivedFunction() {
    const res = await fetch("/showInviteReceived");
    let result = await res.json();
    mainContainer.innerHTML = /*html*/ ``;
    if (result.length === 0) {
        mainContainer.innerHTML = /*html*/ `<div class="select-friend">暫時未有想認識你的人</div>`;
    } else {
        for (let received of result) {
            switch (received.gender) {
                case 1:
                    genderIcon = `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
		<div class="select-friend" onclick="location.href='users.html?id=${received.id}';">
			<div>
				<img class="icon-medium" src="../uploads/${received.icon}" />
			</div>
			<div class="select-friend-info">
				<div class="display-name">${received.display_name}</div>
				<div class="description">${received.description}</div>
				<div class="gender-age">
					<div>${genderIcon}</div>
					<div>${2021 - new Date(received.date_of_birth).getFullYear()}</div>
				</div>
			</div>
		</div>`;
        }
    }
}

async function displayBySentFunction() {
    const res = await fetch("/showInviteSent");
    let result = await res.json();
    mainContainer.innerHTML = /*html*/ ``;
    if (result.length === 0) {
        mainContainer.innerHTML = /*html*/ `<div class="select-friend">暫時未有感興趣的人</div>`;
    } else {
        for (let received of result) {
            switch (received.gender) {
                case 1:
                    genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
            <div class="select-friend" onclick="location.href='users.html?id=${received.id}';">
                <div>
                    <img class="icon-medium" src="../uploads/${received.icon}" />
                </div>
                <div class="select-friend-info">
                    <div class="display-name">${received.display_name}</div>
                    <div class="description">${received.description}</div>
                    <div class="gender-age">
                        <div>${genderIcon}</div>
                        <div>${2021 - new Date(received.date_of_birth).getFullYear()}</div>
                    </div>
                </div>
            </div>`;
        }
    }
}

window.onload = function () {
    sortBy();
    setSorting();
    currentLocation();
};
