export function currentLocation() {
    navigator.geolocation.getCurrentPosition(async (position) => {
        const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
        };
        await fetch("/currentLocation", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(pos),
        });
    });
}
