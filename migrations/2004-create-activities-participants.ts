import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("activities_participants", (table) => {
        table.increments();
        table.integer("activity_id").unsigned().notNullable();
        table.foreign("activity_id").references("activities.id");
        table.integer("participant_id").unsigned().notNullable();
        table.foreign("participant_id").references("users.id");
        table.boolean("is_host").defaultTo(false);
        table.boolean("is_approved").defaultTo(false);
        table.boolean("is_quit").defaultTo(false);
        table.unique(["activity_id", "participant_id"]);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("activities_participants");
}
