import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("passwords", (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.string("username");
        table.string("password");
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("passwords");
}
