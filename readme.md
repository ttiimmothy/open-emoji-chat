# tecky-project: emoji chat

## introduction

一個名副其實嘅交友app加上實測到用家講嘢嘅情緒再利用emoji傳送返比其他用家

&nbsp;

## folder

### <ins>.vscode</ins>

debug launch.json folder

### <ins>admin</ins>

admin and reset password folder

### <ins>controller</ins>

controller and corresponding spec folders

### <ins>env</ins>

.env folder

### <ins>lib</ins>

routes and other lib functions folders

### <ins>migrations</ins>

knex migrations folder

### <ins>private</ins>

after login folder

### <ins>public</ins>

before login folder

### <ins>python</ins>

python model folder

### <ins>routers</ins>

routes.ts inside the folders

### <ins>seeds</ins>

knex seeds folder

### <ins>services</ins>

services and corresponding spec folders

&nbsp;

## Reminders before you run this project

1. open 3 folders in private folder

- guards

- messages

- uploads

2. create .env file in env folder following the .env.sample file

3. create filterTemp.wav file to python/mevonAISpeechEmotionRecognitionmaster/src folder

4. create output folder to python/mevonAISpeechEmotionRecognitionmaster/src folder

5. run following commands

``` Bash
createdb emoji
```

``` Bash
yarn install
```

```Bash
yarn knex migrate:latest
```

```Bash
yarn knex seed:run
```

&nbsp;

## Run this project

type following command line in your terminal

```Bash
yarn dev
```