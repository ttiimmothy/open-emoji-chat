export const my_register_status = Object.freeze({
    used_user: 491,
    used_email: 492,
});

export const createResponse = (success: boolean, status: number, message: string) => {
    return {
        success,
        status,
        message,
    };
};
