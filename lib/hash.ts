import bcrypt from "bcryptjs";
import {logger} from "./logger";

const SALT_ROUNDS = 10;

// Plaintext hash做hashed string
export async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

// 另一段hash咗佢, 再同hashed string比較
export async function checkPassword(plainPassword: string, hashPassword: string) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
}

export async function winston() {
    hashPassword("Rr6248539175").then(logger.debug);
    hashPassword("98qenorI'l'O=0z").then(logger.debug);
    hashPassword("admin@tecky.io").then(logger.debug);
}
