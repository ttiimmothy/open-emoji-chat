export function getDistanceFromLatLonInKm(lat1: number, lon1: number, lat2: number, lon2: number) {
    let R = 6371; // radius of the earth in km
    let latitudeDistance = degToRad(lat2 - lat1); // degToRad below
    let longitudeDistance = degToRad(lon2 - lon1);
    let a =
        Math.sin(latitudeDistance / 2) * Math.sin(latitudeDistance / 2) +
        Math.cos(degToRad(lat1)) *
            Math.cos(degToRad(lat2)) *
            Math.sin(longitudeDistance / 2) *
            Math.sin(longitudeDistance / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // distance in km
    return d;
}

function degToRad(deg: number) {
    return deg * (Math.PI / 180);
}
