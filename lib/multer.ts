import path from "path";
import multer from "multer";

// multer
export const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (
            file.mimetype.split("/")[1] === "png" ||
            file.mimetype.split("/")[1] === "jpeg" ||
            file.mimetype.split("/")[1] === "jpg" ||
            file.mimetype.split("/")[1] === "gif"
        ) {
            cb(null, path.resolve("./private/uploads"));
        } else if (
            file.mimetype.split("/")[1] === "m4a" ||
            file.mimetype.split("/")[1] === "wav" ||
            file.mimetype.split("/")[1] === "opus" ||
            file.mimetype.split("/")[1] === "mp3"
        ) {
            cb(null, path.resolve("./private/messages"));
        } else {
            cb(null, path.resolve("./private/guards"));
        }
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${
                file.mimetype.split("/")[1] === "jpeg"
                    ? "png"
                    : file.mimetype.split("/")[1] === "jpg"
                    ? "png"
                    : file.mimetype.split("/")[1] === "mp3"
                    ? "m4a"
                    : file.mimetype.split("/")[1] === "opus"
                    ? "m4a"
                    : file.mimetype.split("/")[1]
            }`
        );
    },
});
export const upload = multer({storage});