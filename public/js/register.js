import {getCurrentUser} from "./currentuser.js";

const signUp = document.querySelector("#sign-up-form");
const signUpError = document.querySelector(".error");

signUp.addEventListener("submit", async function (event) {
    event.preventDefault();
    if (signUp.password.value !== signUp.confirmPassword.value) {
        signUpError.innerHTML = /*html*/ `<div class="sign-up-error">請確認密碼</div>`;
        return;
    }
    let registerBtn = document.querySelector(".registerBtn");
    registerBtn.innerHTML = /*html*/ `<div class="sign-up-error">正在註冊</div>`;
    const signUpObject = {};
    signUpObject.username = signUp.username.value;
    signUpObject.email = signUp.email.value;
    signUpObject.password = signUp.password.value;

    const res = await fetch("/sendVerify", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(signUpObject),
    });
    const result = await res.json();

    if (res.status === 200) {
        signUpError.innerHTML = /*html*/ ``;
        registerBtn.innerHTML = /*html*/ `
        <div class="alert alert-success" role="alert">
        	<i class="fas fa-check-circle"></i>
        	註冊成功，請檢查電郵😘
        </div>`;
    }
    if (result.status === 491) {
        signUpError.innerHTML = /*html*/ `<div class="sign-up-error">用戶名稱已註冊🤕</div>`;
        registerBtn.innerHTML = /*html*/ `<input type="submit" class="signUp-color" value="註冊" />`;
    }
    if (result.status === 492) {
        signUpError.innerHTML = /*html*/ `<div class="sign-up-error">電郵已註冊🤕</div>`;
        registerBtn.innerHTML = /*html*/ `<input type="submit" class="signUp-color" value="註冊" />`;
    }
});

window.onlog;
getCurrentUser();
