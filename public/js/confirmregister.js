let uploadImageBtn = document.querySelector("#upload-image-btn");
const signUpError = document.querySelector(".error");
let urlParams = new URLSearchParams(window.location.search);
let username = urlParams.get("username");
let uuid = urlParams.get("uuid");

function preview_image(event) {
    let reader = new FileReader();
    reader.onload = function () {
        let output = document.getElementById("output_image");
        output.src = reader.result;
    };
    reader.readAsDataURL(event.currentTarget.files[0]);
}

// Execute a function when the user releases a key on the keyboard
uploadImageBtn.addEventListener("click", function () {
    document.querySelector("#original-upload-btn").click();
});
document.querySelector("#original-upload-btn").addEventListener("input", function () {
    document.getElementById("output_image").type = "image";
});

async function getCurrentUserWithUuidChecking() {
    const res = await fetch("/currentUser");
    const result = await res.json();
    if (result.success) {
        window.location.href = "find.html";
    }
    checkUuid();
}

async function checkUuid() {
    const uuidObject = {};
    uuidObject.username = username;
    uuidObject.uuid = uuid;
    const res = await fetch("/checkUuid", {
        method: "put",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(uuidObject),
    });
    if (res.status === 404) {
        window.location.href = "/";
    }
}

document.querySelector("#confirm-form").addEventListener("submit", async function (event) {
    // 截咗原本個submit
    event.preventDefault();
    // 攞返個form 嘅element 出嚟
    const form = event.currentTarget;
    const formData = new FormData();
    formData.append("username", username);
    formData.append("display_name", form.display_name.value);
    formData.append("gender", form.gender.value);
    formData.append("date_of_birth", form.date_of_birth.value);
    formData.append("orientation", form.orientation.value);
    formData.append("region", form.region.value);
    formData.append("register_status", 2);
    if (form.icon.files[0]) {
        formData.append("icon", form.icon.files[0]);
    }

    if (form.icon.files.length === 0) {
        signUpError.innerHTML = /*html*/ `<div class="sign-up-error">請上載頭像🤕</div>`;
    } else {
        const res = await fetch("/completeRegister", {
            method: "post",
            body: formData, // formData唔需要Content-Type 果個header
        });
        if (res.status === 200) {
            window.location.href = "login.html";
        }
    }
});

window.onload = async function () {
    await getCurrentUserWithUuidChecking();
};
