import {getCurrentUser} from "./currentuser.js";

let forgetPasswordButton = document.querySelector(".forget-pw-button-color");
let forgetPasswordInfo = document.querySelector(".user-login-pw");
let sendEmailButton = document.querySelector(".user-login-button");
let returnLoginPage = document.querySelector(".signUp-button-color");

document.querySelector("#login-form").addEventListener("submit", async (event) => {
    event.preventDefault(); // To prevent the form from submitting synchronously

    const form = event.currentTarget;
    const user = {};

    user.username = form.username.value;
    user.password = form.password.value;

    // create your form object with the form inputs
    const res = await fetch("/login", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
    });

    const result = await res.json(); // String -> Object
    // 成功的話, 改個location
    if (result.success) {
        getCurrentUser();
        window.location.href = "find.html";
    } else {
        document.querySelector(".showWrongMessage").innerHTML = /*html*/ `用戶名稱/密碼不正確`;
    }
    form.reset();
});

forgetPasswordButton.addEventListener("click", function () {
    forgetPasswordInfo.innerHTML = /*html*/ `<input name="email" type="email" placeholder="電郵地址" required="required" class="email"/>`;
    sendEmailButton.innerHTML = /*html*/ `<div class="forget-password"/>傳送</div>`;
    returnLoginPage.innerHTML = /*html*/ `
	<a href="login.html">
		<div class="signUp-color">回到登入</div>
	</a>`;

    document.querySelector(".forget-password").addEventListener("click", async function () {
        const email = {};
        email.email = document.querySelector(".email").value;

        const res = await fetch("/checkEmail", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(email),
        });

        const result = await res.json();
        if (result.success) {
            await fetch("/forgetPassword", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(email),
            });
            document.querySelector(".showWrongMessage").innerHTML = /*html*/ `成功傳送電郵`;
        } else {
            document.querySelector(".showWrongMessage").innerHTML = /*html*/ `此電郵沒有登記資料`;
        }
    });
});

window.onload = async function () {
    await getCurrentUser();
};
