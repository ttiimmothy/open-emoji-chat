import {getCurrentUser} from "./currentuser.js";

window.onload = async function () {
    await getCurrentUser();
};
