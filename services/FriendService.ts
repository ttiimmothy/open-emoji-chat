import {Knex} from "knex";

export class FriendService {
    constructor(private knex: Knex) {}

    async friendList(id: number) {
        return await this.knex
            .select(
                "users.id as id",
                "username",
                "description",
                "display_name",
                "icon",
                this.knex.raw(/*sql*/ `max(friends_messages.created_at) as created_at`)
            )
            .from("users")
            .leftOuterJoin("friends_messages", "users.id", "friends_messages.user_id")
            .leftOuterJoin("friends", "users.id", "friends.user_id")
            .groupBy("users.id", "username", "description", "icon", "display_name")
            .orderBy("created_at", "desc")
            .where("friends.friend_id", id);
    }

    async chatHistory(user_id: number, friendId: number) {
        return await this.knex
            .select(
                "content",
                "friends_messages.created_at",
                "user_id",
                "friend_id",
                "username",
                "display_name",
                "description",
                "icon"
            )
            .from("friends_messages")
            .leftOuterJoin("users", "users.id", "friends_messages.friend_id")
            .orderBy("friends_messages.created_at")
            .where({
                "friends_messages.friend_id": friendId,
                "friends_messages.user_id": user_id,
            })
            .orWhere({
                "friends_messages.friend_id": user_id,
                "friends_messages.user_id": friendId,
            });
    }

    async sendMessage(user_id: number, targetId: number, recordingSet: string) {
        return await this.knex
            .insert({
                user_id: user_id,
                content: recordingSet,
                friend_id: targetId,
            })
            .into("friends_messages");
    }

    async searchMessage(user_id: number, searchWord: string) {
        return await this.knex.select("*").from("users").where("user_id", user_id).andWhere("searchWord", searchWord);
    }
}
